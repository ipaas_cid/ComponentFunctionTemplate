package com.bizcloud.function;

import com.clickpaas.ipaas.cid.component.config.CIDComponentConfig;

/**
    函数组件方法 案例
 */
public class HelloWorld {
    public CIDComponentConfig execute(CIDComponentConfig cidComponentConfig) {
        //todo business
        cidComponentConfig.setMethodResult("hello");
        System.out.println("hello");
        return cidComponentConfig;
    }
}
